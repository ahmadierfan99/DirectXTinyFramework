#include "Window.h"

Window* gWindow = nullptr;

LRESULT CALLBACK gWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	return gWindow->WndProc(hwnd, msg, wParam, lParam);
}


Window::Window(int width, int height, HINSTANCE instance)
{
	WNDCLASSEX wcex;

	ZeroMemory(&wcex, sizeof(WNDCLASSEX));

	LPCTSTR className = (LPCTSTR)"DirectX";

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.cbClsExtra = NULL;
	wcex.style = CS_VREDRAW | CS_HREDRAW;
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
	wcex.hInstance = instance;
	wcex.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wcex.lpszClassName = className;
	wcex.lpfnWndProc = gWndProc;

	HRESULT hr = RegisterClassEx(&wcex);

	if (FAILED(hr))
		PrintError(hr);

	RECT wr = { 0,0,width,height };
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, NULL);

	client_size = { width,height };

	handle = CreateWindowEx(NULL, className,
		L"DirectXApp",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		wr.right - wr.left,
		wr.bottom - wr.top,
		NULL,
		NULL,
		instance,
		NULL);

	InitRenderTarget(handle, client_size);

	ShowWindow(handle, SW_SHOW);

}

LRESULT  Window::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0;
}

void Window::ClearView(FLOAT color[])
{
	D3DDevice::devcon->ClearRenderTargetView(back_buffer.Get(), color);
	D3DDevice::devcon->ClearDepthStencilView(depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}

void Window::Present()
{
	swap_chain->Present(0, 0);
}