#pragma once

#include "RenderTarget.h"

class Window : public RenderTarget
{
public:
	Window(int width, int height, HINSTANCE instance);
	~Window() = default;
	LRESULT WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	void ClearView(FLOAT color[]);
	void Present();
private:
	HWND handle;
};

