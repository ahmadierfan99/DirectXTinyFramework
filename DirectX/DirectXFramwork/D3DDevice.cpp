#include "D3DDevice.h"
#include "VertexShader.h"
#include "PixelShader.h"
#include <corecrt.h>
#include <corecrt_malloc.h>
#include <corecrt_search.h>
#include <corecrt_wstdlib.h>
#include <limits.h>

Device  D3DDevice::dev;
DeviceContext D3DDevice::devcon;

D3DDevice::StaticInit D3DDevice::static_init;

D3DDevice::StaticInit::StaticInit()
{
	CreateDevice(dev, devcon);
}

void D3DDevice::CreateDevice(Device &device, DeviceContext &deviceContext)
{
	UINT createDeviceFlags = 0;
#if defined(DEBUG) || defined(_DEBUG)
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	HRESULT hr = 0;

	hr = D3D11CreateDevice(NULL,
		D3D_DRIVER_TYPE_HARDWARE,
		NULL,
		createDeviceFlags,
		0,
		0,
		D3D11_SDK_VERSION,
		device.GetAddressOf(),
		0,
		deviceContext.GetAddressOf());

	CheckResult(hr);
}

void D3DDevice::CreateSwapChain(SwapChain &swapChain, HWND handle, int clientWidth, int clientHeight)
{
	HRESULT hr = 0;

	UINT msaa_quality;

	hr = dev->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &msaa_quality);

	CheckResult(hr);

	assert(msaa_quality > 0);

	CheckResult(hr);

	DXGI_SWAP_CHAIN_DESC scd;

	ZeroMemory(&scd, sizeof(DXGI_SWAP_CHAIN_DESC));

	scd.BufferCount = 1;
	scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	scd.BufferDesc.Width = clientWidth;
	scd.BufferDesc.Height = clientHeight;
	scd.BufferDesc.RefreshRate.Numerator = 60;
	scd.BufferDesc.RefreshRate.Denominator = 0;
	scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	scd.OutputWindow = handle;
	scd.SampleDesc.Count = 4;
	scd.SampleDesc.Quality = msaa_quality - 1;
	scd.Windowed = true;
	scd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	scd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	scd.Flags = 0;

	CheckResult(hr);

	hr = GetDXGIFactory().Get()->CreateSwapChain(dev.Get(), &scd, swapChain.GetAddressOf());

	CheckResult(hr);
}

void D3DDevice::CreateBackBuffer(SwapChain const &swapChain, RenderTargetView &backBuffer)
{
	Texture2D bb;
	HRESULT hr;

	hr = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)bb.GetAddressOf());

	CheckResult(hr);

	hr = dev->CreateRenderTargetView(bb.Get(), NULL, backBuffer.GetAddressOf());

	CheckResult(hr);
}

void D3DDevice::CreateDepthStencilView(DepthStencilView &depthStencilView, XMINT2 windowSize)
{
	HRESULT hr = 0;

	UINT msaa_quality;

	hr = dev->CheckMultisampleQualityLevels(DXGI_FORMAT_D24_UNORM_S8_UINT, 4, &msaa_quality);

	CheckResult(hr);

	assert(msaa_quality > 0);

	D3D11_TEXTURE2D_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

	depthStencilDesc.Usage = D3D11_USAGE_DEFAULT;
	depthStencilDesc.ArraySize = 1;
	depthStencilDesc.MipLevels = 1;
	depthStencilDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthStencilDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilDesc.Width = windowSize.x;
	depthStencilDesc.Height = windowSize.y;
	depthStencilDesc.SampleDesc.Count = 4;
	depthStencilDesc.SampleDesc.Quality = msaa_quality - 1;
	depthStencilDesc.CPUAccessFlags = 0;
	depthStencilDesc.MiscFlags = 0;

	Texture2D depthStencilBuffer;

	dev->CreateTexture2D(&depthStencilDesc, NULL, depthStencilBuffer.GetAddressOf());
	dev->CreateDepthStencilView(depthStencilBuffer.Get(), NULL, depthStencilView.GetAddressOf());
}

void D3DDevice::CreateVertexShader(VertexShader &vshader)
{
	HRESULT hr = 0;
	hr = dev.Get()->CreateVertexShader(gVShader, sizeof(gVShader), nullptr, vshader.GetAddressOf());
}

void D3DDevice::CreatePixelShader(PixelShader &pshader)
{
	HRESULT hr = 0;
	hr = dev.Get()->CreatePixelShader(gPShader, sizeof(gPShader), nullptr, pshader.GetAddressOf());
}

void D3DDevice::CreateAndSetInputLayout(D3D11_INPUT_ELEMENT_DESC const *ied, UINT numElements,
	const void *bytecode, UINT bytecodeLength, InputLayout &inputLayout)
{
	HRESULT hr = 0;
	hr = dev.Get()->CreateInputLayout(ied, numElements, bytecode, bytecodeLength, inputLayout.GetAddressOf());
	CheckResult(hr);
	devcon.Get()->IASetInputLayout(inputLayout.Get());
}

void D3DDevice::CreateVertexBuffer(Buffer &vertexBuffer, Vertex* vertices, UINT numVertices)
{
	D3D11_BUFFER_DESC vertexBufferDesc;
	ZeroMemory(&vertexBufferDesc, sizeof(vertexBufferDesc));

	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.ByteWidth = sizeof(Vertex) * numVertices;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	D3D11_SUBRESOURCE_DATA vertexData;
	ZeroMemory(&vertexData, sizeof(vertexData));

	vertexData.pSysMem = vertices;

	dev->CreateBuffer(&vertexBufferDesc, &vertexData, vertexBuffer.GetAddressOf());
}

void D3DDevice::CreateIndexBuffer(Buffer& indexBuffer, DWORD* indices, UINT numIndices)
{
	D3D11_BUFFER_DESC indexBufferDesc;
	ZeroMemory(&indexBufferDesc, sizeof(indexBufferDesc));

	indexBufferDesc.ByteWidth = sizeof(DWORD) * numIndices;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA indexBufferData;
	indexBufferData.pSysMem = indices;

	dev->CreateBuffer(&indexBufferDesc, &indexBufferData, indexBuffer.GetAddressOf());
}

void D3DDevice::CreateConstantBuffer(Buffer& constantBuffer, UINT size)
{
	D3D11_BUFFER_DESC constantBufferDesc;
	ZeroMemory(&constantBufferDesc, sizeof(constantBufferDesc));

	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.ByteWidth = size;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.MiscFlags = 0;

	dev->CreateBuffer(&constantBufferDesc, 0, constantBuffer.GetAddressOf());
}

void D3DDevice::SetRenderTargets(RenderTargetView const &backBuffer, DepthStencilView const &depthStencil)
{
	devcon->OMSetRenderTargets(1, backBuffer.GetAddressOf(), depthStencil.Get());
}

void D3DDevice::CreateRS(RasterizerState& rs, D3D11_FILL_MODE fillMode, D3D11_CULL_MODE cullMode, bool frontCCW)
{
	D3D11_RASTERIZER_DESC rasterizerStateDesc;
	ZeroMemory(&rasterizerStateDesc, sizeof(rasterizerStateDesc));

	rasterizerStateDesc.CullMode = cullMode;
	rasterizerStateDesc.FillMode = fillMode;
	rasterizerStateDesc.DepthClipEnable = TRUE;
	rasterizerStateDesc.FrontCounterClockwise = frontCCW;
	HRESULT hr = dev->CreateRasterizerState(&rasterizerStateDesc, rs.GetAddressOf());

	CheckResult(hr);
}

void D3DDevice::CreateRSSolid(RasterizerState& rs)
{
	CreateRS(rs, D3D11_FILL_SOLID, D3D11_CULL_NONE,false);
}

void D3DDevice::CreateRSWireframe(RasterizerState& rs)
{
	CreateRS(rs, D3D11_FILL_WIREFRAME, D3D11_CULL_NONE,false);
}

void D3DDevice::CreateBS(BlendState& bs)
{
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	blendDesc.RenderTarget[0].BlendEnable = true;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_COLOR;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_BLEND_FACTOR;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	blendDesc.AlphaToCoverageEnable = false;

	dev->CreateBlendState(&blendDesc, bs.GetAddressOf());
}

void D3DDevice::CreateViewport(Viewport &viewport, DirectX::XMINT2 &size)
{
	ZeroMemory(&viewport, sizeof(viewport));

	viewport.TopLeftX = 0;
	viewport.TopLeftY = 0;
	viewport.Width = size.x;
	viewport.Height = size.y;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
}

DXGIFactory D3DDevice::GetDXGIFactory()
{
	DXGIDevice dxgi_device;
	DXGIAdapter dxgi_adapter;
	DXGIFactory dxgi_factory;

	HRESULT hr = 0;

	hr = dev.Get()->QueryInterface(__uuidof(IDXGIDevice), (LPVOID*)dxgi_device.GetAddressOf());

	CheckResult(hr);

	hr = dxgi_device.Get()->GetParent(__uuidof(IDXGIAdapter), (LPVOID*)dxgi_adapter.GetAddressOf());

	CheckResult(hr);

	hr = dxgi_adapter.Get()->GetParent(__uuidof(IDXGIFactory), (LPVOID*)dxgi_factory.GetAddressOf());

	CheckResult(hr);

	return dxgi_factory;
}