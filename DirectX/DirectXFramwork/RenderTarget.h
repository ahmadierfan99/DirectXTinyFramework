#pragma once
#include "D3DDevice.h"
#include "Common.h"

class RenderTarget
{
public:
	RenderTarget() = default;
	void InitRenderTarget(HWND handle, DirectX::XMINT2 &size);
	SwapChain GetSwapChain() const;
	RenderTargetView GetRenderTargetView() const;
	DepthStencilView GetDepthStencilView() const;
	Viewport GetViewport() const;
	DirectX::XMINT2 GetWindowSize();
protected:
	DirectX::XMINT2 client_size;
	SwapChain swap_chain;
	RenderTargetView back_buffer;
	DepthStencilView depth_stencil_view;
	Viewport viewport;
};

