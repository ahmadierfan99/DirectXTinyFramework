

struct PSInput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

Texture2D mTexture;
SamplerState mSamplerState;

float4 PShader(PSInput input) : SV_TARGET
{
	return mTexture.Sample(mSamplerState,input.texcoord) * input.color;
}