#include "RenderTarget.h"

void RenderTarget::InitRenderTarget(HWND handle, DirectX::XMINT2 &size)
{
	D3DDevice::CreateSwapChain(swap_chain, handle, size.x, size.y);
	D3DDevice::CreateBackBuffer(swap_chain, back_buffer);
	D3DDevice::CreateDepthStencilView(depth_stencil_view, size);
	D3DDevice::CreateViewport(viewport, size);
}

SwapChain RenderTarget::GetSwapChain() const
{
	return swap_chain;
}

RenderTargetView RenderTarget::GetRenderTargetView() const
{
	return back_buffer;
}

DepthStencilView RenderTarget::GetDepthStencilView() const
{
	return depth_stencil_view;
}

DirectX::XMINT2 RenderTarget::GetWindowSize()
{
	return client_size;
}

Viewport RenderTarget::GetViewport() const
{
	return viewport;
}