#pragma once

#include <Windows.h>
#include <iostream>
#include <vector>
#include <comdef.h>
#include "XTK/Inc/WICTextureLoader.h"
#include <d3d11.h>
#include "wrl/client.h";
#include <DirectXMath.h>
#include <d3dcompiler.h>

using namespace Microsoft::WRL;

using DirectX::XMFLOAT2;
using DirectX::XMFLOAT3;
using DirectX::XMFLOAT4;
using DirectX::XMVECTOR;
using DirectX::XMINT2;
using DirectX::XMVectorSet;
using DirectX::XMMATRIX;

#pragma comment(lib, "DirectXTK.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dx11.lib")
#pragma comment(lib,"d3dcompiler.lib")

#define PrintError(hr) do { _com_error err(hr); std::cerr << err.ErrorMessage() << " " << __FILE__ << " " << __LINE__ << std::endl; } while (false)
#define FAILED(hr) (((HRESULT)(hr)) < 0)
#define SafeRelease(ptr) do { if(ptr) {ptr->Release();} } while(false)
#define CountOf(arr) sizeof(arr) / sizeof(arr[0])

typedef XMFLOAT3 D3DCOLOR;

#define BLACK	 D3DCOLOR{0, 0, 0}
#define WHITE	 D3DCOLOR{1, 1, 1}
#define RED		 D3DCOLOR{1, 0, 0}
#define GREEN	 D3DCOLOR{0, 1, 0}
#define BLUE	 D3DCOLOR{0, 0, 1}

typedef ComPtr<IDXGISwapChain> SwapChain;
typedef ComPtr<ID3D11Device> Device;
typedef ComPtr<ID3D11DeviceContext> DeviceContext;
typedef ComPtr<ID3D11Texture2D> Texture2D;
typedef ComPtr<ID3D11RenderTargetView> RenderTargetView;
typedef ComPtr<ID3D11DepthStencilView> DepthStencilView;
typedef ComPtr<IDXGIDevice> DXGIDevice;
typedef ComPtr<IDXGIAdapter> DXGIAdapter;
typedef ComPtr<IDXGIFactory> DXGIFactory;
typedef ComPtr<IDXGIOutput> DXGIOutput;
typedef ComPtr<ID3D11BlendState> BlendState;
typedef ComPtr<ID3D11DepthStencilState> DepthStencilState;
typedef ComPtr<ID3D11RasterizerState> RasterizerState;
typedef ComPtr<ID3D11SamplerState> SamplerState;
typedef ComPtr<ID3DBlob> Blob;
typedef ComPtr<ID3D11VertexShader> VertexShader;
typedef ComPtr<ID3D11PixelShader> PixelShader;
typedef ComPtr<ID3D11Buffer> Buffer;
typedef ComPtr<ID3D11InputLayout> InputLayout;
typedef ComPtr<ID3D11ShaderResourceView> ShaderResourceView;

typedef D3D11_VIEWPORT Viewport;

struct Vertex
{
	Vertex() : position({ 0,0,0 }), color({ 0,0,0,0 }), texCoord({ 0,0 }) {};
	Vertex(XMFLOAT3 pos, XMFLOAT4 clr, XMFLOAT2 texcrd) : position(pos), color(clr), texCoord(texcrd) {};
	Vertex(float x, float y, float z, XMFLOAT4 clr, XMFLOAT2 texcrd) : position({ x,y,z }), color(clr), texCoord(texcrd) {};

	XMFLOAT3 position;
	XMFLOAT4 color;
	XMFLOAT2 texCoord;
};

struct MeshData
{
public:
	MeshData(std::vector<Vertex> v, std::vector<DWORD> i) : vertices(v), indices(i) {}

	std::vector<Vertex> GetVertexArray()
	{
		return vertices;
	}

	std::vector<DWORD> GetIndexArray()
	{
		return indices;
	}

	UINT GetVertexCount() { return vertices.size(); };
	UINT GetIndexCount() { return indices.size(); };

	float GetAlpha() const;

	std::vector<Vertex> vertices;
	std::vector<DWORD> indices;
};

inline float MeshData::GetAlpha() const
{
	return vertices[0].color.w;
}

inline void CheckResult(HRESULT hr)
{
	if (FAILED(hr))
		PrintError(hr);
}