cbuffer cbPerObject
{
	float4x4 WVP;
};

struct VSInput
{
	float4 position : POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

struct PSInput
{
	float4 position : SV_POSITION;
	float4 color : COLOR;
	float2 texcoord : TEXCOORD;
};

PSInput VShader(VSInput input)
{
	PSInput output;

	//output.position = input.position;
	output.position = mul(input.position,WVP);
	output.color = input.color;
	output.texcoord = input.texcoord;

	return output;
}