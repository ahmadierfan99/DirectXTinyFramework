#include "Window.h"
#include "GameObjectManager.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	Window window(640, 480, hInstance);

	MSG msg = { 0 };

	// GameObjects MUST be created before "GameObjectManager::InitializeBuffers()" because Vertex and other buffers need to be created based on
	// their mesh data.

	GameObjectManager::GameObject obj(CubeMeshData(WHITE, 1.0f),
	{ 0.0f, 0.0f, 10.0f },// Position
	{ 0.0f, 0.0f, 0.0f },// Rotation
	{ 1.0f, 1.0f, 1.0f },// Scale
	L"Textures/texture2.dds"); // Texture FileDir

	BlendState bs;
	D3DDevice::CreateBS(bs);

	GameObjectManager::InitializeBuffers();
	FLOAT clearColor[4] = { 0.15f,0.15f,0.70f,1 };
	float diagonalLength = 0;
	float m = 0.0001;
	while (msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
			DispatchMessage(&msg);
		else
		{
			window.ClearView(clearColor);
			GameObjectManager::Draw(window, bs);
			static float x = 10, y = 10, z = 10;
			x += m*1;
			y += m*-2;
			z += m*1;
			diagonalLength = sqrt((x*x) + (y*y) + (z*z));
			obj.SetScale(x, y, z);
			obj.Rotate(0, 0.0001, 0);
			std::cout << diagonalLength;
			window.Present();
		}
	}

	return msg.wParam;
}