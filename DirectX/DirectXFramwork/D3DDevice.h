#pragma once
#include "Common.h"

class D3DDevice
{
public:
	D3DDevice() = delete;
	~D3DDevice() = delete;

	static Device  dev;
	static DeviceContext devcon;

	static void CreateDevice(Device &device, DeviceContext &deviceContext);
	static void CreateSwapChain(SwapChain &swapChain, HWND handle, int clientWidth, int clientHeight);
	static void CreateBackBuffer(SwapChain const &swapChain, RenderTargetView &backBuffer);
	static void CreateDepthStencilView(DepthStencilView &depthStencilView, DirectX::XMINT2 windowSize);
	static void CreateVertexShader(VertexShader &vshader);
	static void CreatePixelShader(PixelShader &pshader);
	static void CreateAndSetInputLayout(D3D11_INPUT_ELEMENT_DESC const *ied, UINT numElements,
		const void *bytecode, UINT bytecodeLength, InputLayout &inputLayout);

	static void CreateVertexBuffer(Buffer &vertexBuffer, Vertex *vertices, UINT numVertices);
	static void CreateIndexBuffer(Buffer& indexBuffer, DWORD* indices, UINT numIndices);
	static void CreateConstantBuffer(Buffer& constantBuffer, UINT size);
	static void CreateViewport(Viewport &viewport, DirectX::XMINT2 &size);

	static void SetRenderTargets(RenderTargetView const &backBuffer, DepthStencilView const &depthStencil);

	static void CreateRS(RasterizerState& rs, D3D11_FILL_MODE fillMode, D3D11_CULL_MODE cullMode, bool frontCCW);
	static void CreateRSSolid(RasterizerState& rs);
	static void CreateRSWireframe(RasterizerState& rs);

	static void CreateBS(BlendState &bs);

	static DXGIFactory GetDXGIFactory();
private:
	class StaticInit
	{
	public:
		StaticInit();
	};

	static StaticInit static_init;
};

