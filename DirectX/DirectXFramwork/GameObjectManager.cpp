#include "GameObjectManager.h"
#include "D3DDevice.h"
#include "VertexShader.h"
#include <algorithm>


VertexShader GameObjectManager::vs;
PixelShader GameObjectManager::ps;
InputLayout GameObjectManager::input_layout;
Buffer GameObjectManager::vertex_buffer;
Buffer GameObjectManager::index_buffer;
Buffer GameObjectManager::constant_buffer;
vector<GameObjectManager::GameObject*> GameObjectManager::game_objects;
UINT GameObjectManager::indices_count;

RasterizerState GameObjectManager::CW;
RasterizerState GameObjectManager::CCW;

XMMATRIX GameObjectManager::camera_view;
XMMATRIX GameObjectManager::camera_projection;
GameObjectManager::cbPerObject GameObjectManager::cb_per_object;

GameObjectManager::Camera  GameObjectManager::GlobalCamera
(
	XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f),
	XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f),
	XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f)
	);

GameObjectManager::StaticInit GameObjectManager::static_init;

GameObjectManager::StaticInit::StaticInit()
{
	D3DDevice::CreateVertexShader(vs);
	D3DDevice::CreatePixelShader(ps);

	D3D11_INPUT_ELEMENT_DESC ied[]
	{
		{"POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"COLOR",0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0},
		{"TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT,D3D11_INPUT_PER_VERTEX_DATA,0}
	};

	D3DDevice::CreateAndSetInputLayout(ied, CountOf(ied), gVShader, sizeof(gVShader), input_layout);
}

void GameObjectManager::Prepare(RenderTarget& renderTarget,BlendState &blendState)
{
	Viewport viewport = renderTarget.GetViewport();
	D3DDevice::devcon.Get()->RSSetViewports(1, &viewport);
	D3DDevice::SetRenderTargets(renderTarget.GetRenderTargetView(), renderTarget.GetDepthStencilView());
	D3DDevice::devcon.Get()->VSSetShader(vs.Get(), 0, 0);
	D3DDevice::devcon.Get()->PSSetShader(ps.Get(), 0, 0);
	UINT strides = sizeof(Vertex);
	UINT offsets = 0;
	D3DDevice::devcon->IASetVertexBuffers(0, 1, vertex_buffer.GetAddressOf(), &strides, &offsets);
	D3DDevice::devcon->IASetIndexBuffer(index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
	D3DDevice::devcon->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void GameObjectManager::Draw(RenderTarget& renderTarget, BlendState &blendState)
{
	Prepare(renderTarget, blendState);

	camera_view = DirectX::XMMatrixLookAtLH(GlobalCamera.GetPosition(), GlobalCamera.GetTarget(), GlobalCamera.GetCameraUp());
	camera_projection = DirectX::XMMatrixPerspectiveFovLH(0.33f * 3.14f, (float)renderTarget.GetWindowSize().x / renderTarget.GetWindowSize().y,
		1.0f, 1000.0f);
	//camera_projection = DirectX::XMMatrixOrthographicLH(renderTarget.GetWindowSize().x, renderTarget.GetWindowSize().y, 1.0f, 1000.0f);

	UINT indBegin = 0;

	for (auto gameObject : game_objects)
	{
		XMMATRIX World = DirectX::XMMatrixIdentity();

		XMMATRIX Scale = DirectX::XMMatrixScaling(gameObject->GetScale().x, gameObject->GetScale().y,
			gameObject->GetScale().z);

		XMVECTOR rotationVector = XMVectorSet(gameObject->GetRotation().x, gameObject->GetRotation().y, gameObject->GetRotation().z, 1);

		XMMATRIX Rotation = DirectX::XMMatrixRotationRollPitchYawFromVector(rotationVector);

		XMMATRIX Translation = DirectX::XMMatrixTranslation(gameObject->GetPosition().x,
			gameObject->GetPosition().y, gameObject->GetPosition().z);

		World = Scale  * Rotation * Translation;

		XMMATRIX WVP = World * camera_view * camera_projection;

		cb_per_object.WVP = DirectX::XMMatrixTranspose(WVP);
		D3DDevice::devcon->UpdateSubresource(constant_buffer.Get(), 0, NULL, &cb_per_object, 0, 0);
		D3DDevice::devcon->VSSetConstantBuffers(0, 1, constant_buffer.GetAddressOf());

		float alpha = gameObject->GetMeshData().GetAlpha();
		float factor[] = { 1 - alpha, 1 - alpha, 1 - alpha, 1.0f };
		D3DDevice::devcon->OMSetBlendState(blendState.Get(), factor, 0xffffffff);

		D3DDevice::devcon->PSSetShaderResources(0, 1, gameObject->GetShaderResourceView().GetAddressOf());
		D3DDevice::devcon->PSSetSamplers(0, 1, gameObject->GetSamplerState().GetAddressOf());

		UINT objIndCount = gameObject->GetMeshData().GetIndexCount();

		D3DDevice::devcon->RSSetState(CCW.Get());
		D3DDevice::devcon->DrawIndexed(objIndCount, indBegin, 0);
		D3DDevice::devcon->RSSetState(CW.Get());
		D3DDevice::devcon->DrawIndexed(objIndCount, indBegin, 0);

		indBegin += objIndCount;
	}
}

void GameObjectManager::InitializeBuffers()
{
	vector<Vertex> all_verts;
	vector<DWORD> all_inds;

	int last_vert_count = 0;
	for (auto go : game_objects)
	{
		vector<Vertex> v = go->GetMeshData().GetVertexArray();
		all_verts.insert(all_verts.end(), v.begin(), v.end());

		for (auto ind : go->GetMeshData().GetIndexArray())
		{
			all_inds.push_back(ind + last_vert_count);
		}

		last_vert_count += go->GetMeshData().GetVertexCount();
	}


	indices_count = all_inds.size();

	D3DDevice::CreateVertexBuffer(vertex_buffer, all_verts.data(), all_verts.size());
	D3DDevice::CreateIndexBuffer(index_buffer, all_inds.data(), indices_count);
	D3DDevice::CreateConstantBuffer(constant_buffer, sizeof(cbPerObject));

	std::sort(game_objects.begin(),game_objects.end(),[](GameObject* go1, GameObject* go2)
	{
		return go1->GetMeshData().GetAlpha() > go2->GetMeshData().GetAlpha();
	});

	D3DDevice::CreateRS(CW, D3D11_FILL_SOLID, D3D11_CULL_BACK,false);
	D3DDevice::CreateRS(CCW, D3D11_FILL_SOLID, D3D11_CULL_BACK,true);
}

GameObjectManager::GameObject::GameObject(MeshData md, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale) :
	mesh_data(md), position(pos), rotation(rot), scale(scale)
{
	GameObjectManager::Add(*this);
}

GameObjectManager::GameObject::GameObject(MeshData md, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale, const wchar_t* textureName) :
	mesh_data(md), position(pos), rotation(rot), scale(scale)
{
	GameObjectManager::Add(*this);

	HRESULT hr = DirectX::CreateWICTextureFromFile(D3DDevice::dev.Get(), D3DDevice::devcon.Get(), textureName, nullptr, texture.GetAddressOf());
	CheckResult(hr);

	D3D11_SAMPLER_DESC samplerDesc;
	ZeroMemory(&samplerDesc, sizeof(samplerDesc));

	float borderColor[] = { 0, 0, 0, 0 };

	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	samplerDesc.MinLOD = -FLT_MAX;
	samplerDesc.MaxLOD = FLT_MAX;

	hr = D3DDevice::dev->CreateSamplerState(&samplerDesc, textureSampler.GetAddressOf());
	CheckResult(hr);
}

void GameObjectManager::GameObject::Translate(DirectX::XMFLOAT3 _position)
{
	position.x += _position.x;
	position.y += _position.y;
	position.z += _position.z;
}

void GameObjectManager::GameObject::Translate(float x, float y, float z)
{
	position.x += x;
	position.y += y;
	position.z += z;
}

void GameObjectManager::GameObject::Rotate(XMFLOAT3 _rotation)
{
	rotation.x += _rotation.x;
	rotation.y += _rotation.y;
	rotation.z += _rotation.z;
}

void GameObjectManager::GameObject::Rotate(float x, float y, float z)
{
	rotation.x += x;
	rotation.y += y;
	rotation.z += z;
}

void GameObjectManager::GameObject::SetPosition(XMFLOAT3 _position)
{
	position = _position;
}

void GameObjectManager::GameObject::SetPosition(float x, float y, float z)
{
	position = { x,y,z };
}

void GameObjectManager::GameObject::SetRotation(XMFLOAT3 _rotation)
{
	rotation = _rotation;
}

void GameObjectManager::GameObject::SetRotation(float x, float y, float z)
{
	rotation = { x,y,z };
}

void GameObjectManager::GameObject::SetScale(XMFLOAT3 _scale)
{
	scale = _scale;
}

void GameObjectManager::GameObject::SetScale(float x, float y, float z)
{
	scale = { x,y,z };
}

ShaderResourceView GameObjectManager::GameObject::GetShaderResourceView() const
{
	return texture;
}

SamplerState GameObjectManager::GameObject::GetSamplerState() const
{
	return textureSampler;
}

MeshData GameObjectManager::GameObject::GetMeshData() const
{
	return mesh_data;
}

void GameObjectManager::Add(GameObject &gameObject)
{
	game_objects.push_back(&gameObject);
}

GameObjectManager::Camera::Camera(XMVECTOR camPos, XMVECTOR camTarget, XMVECTOR camUp) : position(camPos), target(camTarget), camera_up(camUp) {}

void GameObjectManager::Camera::SetPosition(XMVECTOR camPos)
{
	position = camPos;
}

void GameObjectManager::Camera::SetTarget(XMVECTOR camTarget)
{
	target = camTarget;
}

void GameObjectManager::Camera::SetCameraUp(XMVECTOR camUp)
{
	camera_up = camUp;
}

XMVECTOR GameObjectManager::Camera::GetPosition() const
{
	return position;
}

XMVECTOR GameObjectManager::Camera::GetTarget() const
{
	return target;
}

XMVECTOR GameObjectManager::Camera::GetCameraUp() const
{
	return camera_up;
};

XMFLOAT3 GameObjectManager::GameObject::GetPosition() const
{
	return position;
}

XMFLOAT3 GameObjectManager::GameObject::GetRotation() const
{
	return rotation;
}

XMFLOAT3 GameObjectManager::GameObject::GetScale() const
{
	return scale;
}