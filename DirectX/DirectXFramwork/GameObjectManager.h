#pragma once

#include "Common.h"
#include "RenderTarget.h"


using std::vector;

class GameObjectManager
{
public:
	GameObjectManager() = delete;
	~GameObjectManager() = delete;

	static void Prepare(RenderTarget& renderTarget, BlendState &blendState);
	static void Draw(RenderTarget &renderTarget, BlendState &blendState);
	static void InitializeBuffers();

	class GameObject
	{
	public:
		GameObject(MeshData md, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
		GameObject(MeshData md, XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale, const wchar_t* textureName);

		void Translate(XMFLOAT3 _position);
		void Translate(float x, float y, float z);

		void Rotate(XMFLOAT3 _rotation);
		void Rotate(float x, float y, float z);

		void SetPosition(XMFLOAT3 _position);
		void SetPosition(float x, float y, float z);

		void SetRotation(XMFLOAT3 _rotation);
		void SetRotation(float x, float y, float z);

		void SetScale(XMFLOAT3 _scale);
		void SetScale(float x, float y, float z);

		ShaderResourceView GetShaderResourceView() const;
		SamplerState GetSamplerState() const;

		MeshData GetMeshData() const;
		XMFLOAT3 GetPosition() const;
		XMFLOAT3 GetRotation() const;
		XMFLOAT3 GetScale() const;

	protected:
		MeshData mesh_data;
		XMFLOAT3 position;
		XMFLOAT3 rotation;
		XMFLOAT3 scale;

		ShaderResourceView  texture;
		SamplerState textureSampler;
	};

	static void Add(GameObject &gameObject);

	class Camera
	{
	public:
		Camera(XMVECTOR camPos, XMVECTOR  camTarget, XMVECTOR  camUp);

		void SetPosition(XMVECTOR camPos);
		void SetTarget(XMVECTOR camTarget);
		void SetCameraUp(XMVECTOR camUp);

		XMVECTOR GetPosition() const;
		XMVECTOR GetTarget() const;
		XMVECTOR GetCameraUp() const;
	protected:
		XMVECTOR position, target, camera_up;
	};

	static Camera GlobalCamera;

private:

	struct cbPerObject
	{
		XMMATRIX WVP;
	};

	static VertexShader vs;
	static PixelShader ps;
	static InputLayout input_layout;
	static Buffer vertex_buffer;
	static Buffer index_buffer;
	static Buffer constant_buffer;

	static vector<GameObject*> game_objects;
	static UINT indices_count;

	static RasterizerState CW;
	static RasterizerState CCW;

	static XMMATRIX camera_view;
	static XMMATRIX camera_projection;
	static cbPerObject cb_per_object;

	class StaticInit
	{
	public:
		StaticInit();
	};

	static StaticInit static_init;
};


inline MeshData QuadMeshData(XMFLOAT3 color, float alpha)
{
	return MeshData(
	{
		Vertex({-0.5f, +0.5f, 0.0f},{ color.x, color.y, color.z, alpha },{0, 0}),
		Vertex({+0.5f, +0.5f, 0.0f},{ color.x, color.y, color.z, alpha },{1, 0}),
		Vertex({-0.5f, -0.5f, 0.0f},{ color.x, color.y, color.z, alpha },{0, 1}),
		Vertex({+0.5f, -0.5f, 0.0f},{ color.x, color.y, color.z, alpha },{1, 1})
	}
		,
		{
			0,1,2,
			1,3,2
		}
		);
}

inline MeshData CubeMeshData(XMFLOAT3 color, float alpha)
{
	return MeshData(
	{
		// Front Face
		Vertex({-1.0f, -1.0f, -1.0f},{ color.x, color.y, color.z, alpha },{0.0f, 1.0f}),
		Vertex({-1.0f,  1.0f, -1.0f},{ color.x, color.y, color.z, alpha },{0.0f, 0.0f}),
		Vertex({1.0f,  1.0f, -1.0f}, { color.x, color.y, color.z, alpha },{1.0f, 0.0f}),
		Vertex({1.0f, -1.0f, -1.0f}, { color.x, color.y, color.z, alpha },{1.0f, 1.0f}),

		// Back Face
		Vertex({-1.0f, -1.0f, 1.0f},{ color.x, color.y, color.z, alpha },{ 1.0f, 1.0f }),
		Vertex({1.0f, -1.0f, 1.0f}, { color.x, color.y, color.z, alpha }, { 0.0f, 1.0f }),
		Vertex({1.0f, 1.0f, 1.0f},  { color.x, color.y, color.z, alpha }, { 0.0f, 0.0f }),
		Vertex({-1.0f, 1.0f, 1.0f}, { color.x, color.y, color.z, alpha }, { 1.0f, 0.0f }),

		// Top Face
		Vertex({-1.0f, 1.0f, -1.0f}, { color.x, color.y, color.z, alpha },{0.0f, 1.0f}),
		Vertex({-1.0f, 1.0f,  1.0f}, { color.x, color.y, color.z, alpha },{0.0f, 0.0f}),
		Vertex({1.0f, 1.0f,  1.0f }, { color.x, color.y, color.z, alpha },{1.0f, 0.0f}),
		Vertex({1.0f, 1.0f, -1.0f }, { color.x, color.y, color.z, alpha },{1.0f, 1.0f}),

		// Bottom Face
		Vertex({-1.0f, -1.0f, -1.0f},{ color.x, color.y, color.z, alpha },{1.0f, 1.0f}),
		Vertex({1.0f, -1.0f, -1.0f}, { color.x, color.y, color.z, alpha } ,{0.0f, 1.0f}),
		Vertex({1.0f, -1.0f,  1.0f}, { color.x, color.y, color.z, alpha } ,{0.0f, 0.0f}),
		Vertex({-1.0f, -1.0f,  1.0f},{ color.x, color.y, color.z, alpha },{1.0f, 0.0f}),

		// Left{ Face			   	 			
		Vertex({-1.0f, -1.0f,  1.0f},{ color.x, color.y, color.z, alpha },{ 0.0f, 1.0}),
		Vertex({-1.0f,  1.0f,  1.0f},{ color.x, color.y, color.z, alpha },{ 0.0f, 0.0}),
		Vertex({-1.0f,  1.0f, -1.0f},{ color.x, color.y, color.z, alpha },{ 1.0f, 0.0}),
		Vertex({-1.0f, -1.0f, -1.0f},{ color.x, color.y, color.z, alpha },{ 1.0f, 1.0}),

		// Righ{t Face			   	 		
		Vertex({1.0f, -1.0f, -1.0f}, { color.x, color.y, color.z, alpha },{0.0f, 1.0f}),
		Vertex({1.0f,  1.0f, -1.0f}, { color.x, color.y, color.z, alpha },{0.0f, 0.0f}),
		Vertex({1.0f,  1.0f,  1.0f}, { color.x, color.y, color.z, alpha },{1.0f, 0.0f}),
		Vertex({1.0f, -1.0f,  1.0f}, { color.x, color.y, color.z, alpha },{1.0f, 1.0f}),
	}
		,
		{
			// Front Face
			0,  1,  2,
			0,  2,  3,

		// Back Face
		4,  5,  6,
		4,  6,  7,

		// Top Face
		8,  9, 10,
		8, 10, 11,

		// Bottom Face
		12, 13, 14,
		12, 14, 15,

		// Left Face
		16, 17, 18,
		16, 18, 19,

		// Right Face
		20, 21, 22,
		20, 22, 23
		}
		);
}